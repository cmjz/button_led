/*
利用中断判断按键类型，实现判断出长按，短按
长按开关灯 短按切换颜色，混和出橙，紫，粉，白，蓝，红，绿
*/
#include <stdbool.h>
#include <stdint.h>
#include "nrf.h"
#include "nrf_gpiote.h"
#include "nrf_gpio.h"
#include "boards.h"
#include "nrf_drv_ppi.h"
#include "nrf_drv_timer.h"
#include "nrf_drv_gpiote.h"
#include "app_error.h"

#define BLUE_LED  18
#define RED_LED  20
#define GREEN_LED  19

int main()
{
	nrf_gpio_cfg_output(BLUE_LED);
	
	while(true)
	{
		nrf_gpio_pin_clear(BLUE_LED);
	}

}


